from conans import ConanFile, CMake, tools
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout


class UtkConan(ConanFile):
    name = "utk"
    version = "0.3.1"

    license = "Apache License 2.0"
    author = "Innokentiy Alaytsev <alaitsev@gmail.com>"
    url = "https://gitlab.com/UtilityToolKit/utk"
    description = "Miscellaneous utility classes, functions and macros library"
    topics = ("pimpl", "lockfree", "macros")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "CMakeToolchain"

    exports_sources = "*"

    def build_requirements(self):
        self.tool_requires("cmake/[>=3.21.0]")
        self.tool_requires("ninja/[>=1.10.2]")

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def layout(self):
        cmake_layout(self)

    def generate(self):
        tc = CMakeToolchain(self, generator="Ninja")

        tc.generate()

    def build(self):
        cmake = CMake(self)

        variables = {
            "utk_BUILD_SHARED_LIBRARY": str(self.options.shared),
            "utk_BUILD_STATIC_LIBRARY": str(not self.options.shared)}

        cmake.configure(variables=variables)

        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["utk"]
