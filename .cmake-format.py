# -----------------------------
# Options affecting formatting.
# -----------------------------
with section("format"):

  # Disable formatting entirely, making cmake-format a no-op
  disable = False

  # How wide to allow formatted cmake files
  line_width = 80

  # How many spaces to tab for indent
  tab_size = 2

  # If true, lines are indented using tab characters (utf-8 0x09) instead of
  # <tab_size> space characters (utf-8 0x20). In cases where the layout would
  # require a fractional tab character, the behavior of the  fractional
  # indentation is governed by <fractional_tab_policy>
  use_tabchars = False

  # If <use_tabchars> is True, then the value of this variable indicates how
  # fractional indentions are handled during whitespace replacement. If set to
  # 'use-space', fractional indentation is left as spaces (utf-8 0x20). If set
  # to `round-up` fractional indentation is replaced with a single tab character
  # (utf-8 0x09) effectively shifting the column to the next tabstop
  fractional_tab_policy = 'use-space'

  # If an argument group contains more than this many sub-groups (parg or kwarg
  # groups) then force it to a vertical layout.
  max_subgroups_hwrap = 2

  # If a positional argument group contains more than this many arguments, then
  # force it to a vertical layout.
  max_pargs_hwrap = 6

  # If a cmdline positional group consumes more than this many lines without
  # nesting, then invalidate the layout (and nest)
  max_rows_cmdline = 2

  # If true, separate flow control names from their parentheses with a space
  separate_ctrl_name_with_space = True

  # If true, separate function names from parentheses with a space
  separate_fn_name_with_space = True

  # If a statement is wrapped to more than one line, than dangle the closing
  # parenthesis on its own line.
  dangle_parens = True

  # If the trailing parenthesis must be 'dangled' on its on line, then align it
  # to this reference: `prefix`: the start of the statement,  `prefix-indent`:
  # the start of the statement, plus one indentation  level, `child`: align to
  # the column of the arguments
  dangle_align = 'prefix'

  # If the statement spelling length (including space and parenthesis) is
  # smaller than this amount, then force reject nested layouts.
  min_prefix_chars = 4

  # If the statement spelling length (including space and parenthesis) is larger
  # than the tab width by more than this amount, then force reject un-nested
  # layouts.
  max_prefix_chars = 10

  # If a candidate layout is wrapped horizontally but it exceeds this many
  # lines, then reject the layout.
  max_lines_hwrap = 2

  # What style line endings to use in the output.
  line_ending = 'unix'

  # Format command names consistently as 'lower' or 'upper' case
  command_case = 'canonical'

  # Format keywords consistently as 'lower' or 'upper' case
  keyword_case = 'unchanged'

  # A list of command names which should always be wrapped
  always_wrap = []

  # If true, the argument lists which are known to be sortable will be sorted
  # lexicographicall
  enable_sort = True

  # If true, the parsers may infer whether or not an argument list is sortable
  # (without annotation).
  autosort = False

  # By default, if cmake-format cannot successfully fit everything into the
  # desired linewidth it will apply the last, most agressive attempt that it
  # made. If this flag is True, however, cmake-format will print error, exit
  # with non-zero status code, and write-out nothing
  require_valid_layout = False

  # A dictionary mapping layout nodes to a list of wrap decisions. See the
  # documentation for more information.
  layout_passes = {}


# -------------------------------
# Options affecting file encoding
# -------------------------------
with section("encode"):

  # If true, emit the unicode byte-order mark (BOM) at the start of the file
  emit_byteorder_mark = False

  # Specify the encoding of the input file. Defaults to utf-8
  input_encoding = 'utf-8'

  # Specify the encoding of the output file. Defaults to utf-8. Note that cmake
  # only claims to support utf-8 so be careful when using anything else
  output_encoding = 'utf-8'


# ----------------------------------
# Options affecting listfile parsing
# ----------------------------------
with section("parse"):
  additional_commands = {
    "conan_cmake_run": {
      "kwargs": {
        "CONANFILE": '1',
        "BUILD": '+',
        "BUILD_TYPE": '1',
        "CONFIGURATION_TYPES": '1',
        "SETTINGS": '1'
      }
    },
    "utk_cmake_add_library_targets": {
      "kwargs": {
        "BUILD_SHARED_LIBRARY": '1',
        "BUILD_STATIC_LIBRARY": '1',
        "DEFAULT_TARGET_TYPE": '1',
        "DEFAULT_TARGET_NAME": '1',
        "DEFAULT_TARGET_EXPORTED_NAME": '1'
      }
    },
    "utk_cmake_generate_export_header": {
      "kwargs": {
        "TARGET": '+',
        "COMMON_BASE_NAME": '1'
      }
    },
    "utk_cmake_install_project": {
      "kwargs": {
        "TARGET": '+',
        "INSTALL_DEVEL": '1',
        "INSTALL_SOURCES": '1',
        "CUSTOM_CMAKE_CONFIG_FILE": '1',
        "CMAKE_CONFIG_FILE_OPTIONS": {
          "kwargs": {
            "INSTALL_DESTINATION": '1'
          }
        },
        "CMAKE_CONFIG_VERSION_FILE_OPTIONS": {
          "kwargs": {
            "COMPATIBILITY": '1'
          }
        },
        "CMAKE_PACKAGE_FILE_OPTIONS": {
          "kwargs": {
            "DESTINATION": '1'
          }
        }
      }
    },
    "utk_cmake_name_mangling_postfix": {
      "kwargs": {
        "TARGET": '+',
        "TARGET_VERSION_MANGLING": '1'
      }
    },
    "utk_cmake_product_information": {
      "flags": [ "CREATE_PRODUCT_INFO_FUNCTIONS" ],
      "kwargs": {
        "TARGET": '+',
        "COMMON_TARGET_IDENTIFIER": '1',
        "NAME_STRING": '1',
        "COPYRIGHT_HOLDER_FILE": '1',
        "COPYRIGHT_INFO_FILE": '1',
        "COPYRIGHT_INFO_UTF8_FILE": '1'
      }
    },
    "utk_cmake_target_source_group": {
      "kwargs": {
        "TARGET": '1',
        "ROOT_DIR": '1',
        "PREFIX": '1'
      }
    },
    "utk_cmake_target_include_directories": {
      "kwargs": {
        "TARGET": '+',
        "INTERFACE": {
          "pargs": '+'
        },
        "PRIVATE": {
          "pargs": '+'
        },
        "PUBLIC": {
          "pargs": '+'
        }
      }
    },
    "windeployqt": {
      "flags": [ "COMPILER_RUNTIME", "FORCE" ],
      "kwargs": {
        "TARGET": '1',
        "DESTINATION": '1',
        "INCLUDE_MODULES": '+',
        "EXCLUDE_MODULES": '+',
        "QML_DIR": '+',
        "QML_IMPORT": '+'
      }
    }
  }
