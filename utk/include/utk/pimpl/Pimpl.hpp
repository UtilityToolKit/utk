// Copyright 2017-2023 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk/include/utk/pimpl/Pimpl.hpp
//
// Description: Provides definition of the PIMPL holder class.


#ifndef UTK_INCLUDE_UTK_PIMPL_PIMPL_HPP
#define UTK_INCLUDE_UTK_PIMPL_PIMPL_HPP


#include <memory>
#include <type_traits>

#include "utk/namespace.hpp"


UTK_NAMESPACE_OPEN


/**
   @note Based on Herb Sutter pimpl implementation from GotW #101, Part 2
   https://herbsutter.com/gotw/_101/
*/
template < class TImpl >
class Pimpl {
 public:
	Pimpl ();
	Pimpl (Pimpl&& i_other);

	template < class... TArgs_ >
	explicit Pimpl (TArgs_&&... i_args);

	template <
	    class TOtherImpl_,
	    class TIsBaseOf_ =
	        std::enable_if_t< std::is_base_of_v< TImpl, TOtherImpl_ > > >
	explicit Pimpl (TOtherImpl_* i_src);

	~Pimpl ();

	template <
	    class TOtherImpl_,
	    class TIsBaseOf_ =
	        std::enable_if_t< std::is_base_of_v< TImpl, TOtherImpl_ > > >
	TOtherImpl_* As () noexcept;

	template <
	    class TOtherImpl_,
	    class TIsBaseOf_ =
	        std::enable_if_t< std::is_base_of_v< TImpl, TOtherImpl_ > > >
	const TOtherImpl_* As () const noexcept;

	TImpl*       GetPtr () noexcept;
	const TImpl* GetPtr () const noexcept;

	TImpl&       GetRef () noexcept;
	const TImpl& GetRef () const noexcept;

	TImpl*       operator->() noexcept;
	const TImpl* operator->() const noexcept;

	TImpl&       operator* () noexcept;
	const TImpl& operator* () const noexcept;

	Pimpl< TImpl >& operator= (Pimpl< TImpl >&& i_src);


 private:
	std::unique_ptr< TImpl > pimpl_;
};


UTK_NAMESPACE_CLOSE


#endif /* UTK_INCLUDE_UTK_PIMPL_PIMPL_HPP */
