// Copyright 2017-2023 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk/include/utk/pimpl/Pimpl_impl.hpp
//
// Description: Provides definition of the PIMPL holder class.


#ifndef UTK_INCLUDE_UTK_PIMPL_PIMPL_IMPL_HPP
#define UTK_INCLUDE_UTK_PIMPL_PIMPL_IMPL_HPP


#include <utility>

#include "utk/pimpl/Pimpl.hpp"


UTK_NAMESPACE_OPEN


template < class TImpl >
Pimpl< TImpl >::Pimpl ()
    : pimpl_ {std::make_unique< TImpl > ()}
{
}


template < class TImpl >
Pimpl< TImpl >::Pimpl (Pimpl< TImpl >&& i_other)
    : pimpl_ {std::move (i_other.pimpl_)}
{
}


template < class TImpl >
template < class... TArgs_ >
Pimpl< TImpl >::Pimpl (TArgs_&&... i_args)
    : pimpl_ {std::make_unique< TImpl > (std::forward< TArgs_ > (i_args)...)}
{
}


template < class TImpl >
template < class TOtherImpl_, class TIsBaseOf_ >
Pimpl< TImpl >::Pimpl (TOtherImpl_* i_src)
    : pimpl_ {i_src}
{
}


template < class TImpl >
Pimpl< TImpl >::~Pimpl ()
{
}


template < class TImpl >
template < class TOtherImpl_, class TIsBaseOf_ >
TOtherImpl_* Pimpl< TImpl >::As () noexcept
{
	return static_cast< TOtherImpl_* > (GetPtr ());
}


template < class TImpl >
template < class TOtherImpl_, class TIsBaseOf_ >
const TOtherImpl_* Pimpl< TImpl >::As () const noexcept
{
	return const_cast< Pimpl< TImpl >* > (this)->As< TOtherImpl_ > ();
}


template < class TImpl >
TImpl* Pimpl< TImpl >::GetPtr () noexcept
{
	return pimpl_.get ();
}


template < class TImpl >
const TImpl* Pimpl< TImpl >::GetPtr () const noexcept
{
	return const_cast< Pimpl< TImpl >* > (this)->GetPtr ();
}


template < class TImpl >
TImpl& Pimpl< TImpl >::GetRef () noexcept
{
	return *pimpl_.get ();
}


template < class TImpl >
const TImpl& Pimpl< TImpl >::GetRef () const noexcept
{
	return const_cast< Pimpl< TImpl >* > (this)->GetRef ();
}


template < class TImpl >
TImpl* Pimpl< TImpl >::operator->() noexcept
{
	return GetPtr ();
}


template < class TImpl >
const TImpl* Pimpl< TImpl >::operator->() const noexcept
{
	return GetPtr ();
}


template < class TImpl >
TImpl& Pimpl< TImpl >::operator* () noexcept
{
	return GetRef ();
}


template < class TImpl >
const TImpl& Pimpl< TImpl >::operator* () const noexcept
{
	return GetRef ();
}


template < class TImpl >
Pimpl< TImpl >& Pimpl< TImpl >::operator= (Pimpl< TImpl >&& i_src)
{
	pimpl_ = ::std::move (i_src.pimpl_);

	return *this;
}


UTK_NAMESPACE_CLOSE


#endif /* UTK_INCLUDE_UTK_PIMPL_PIMPL_IMPL_HPP */
