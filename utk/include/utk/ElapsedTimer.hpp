// Copyright 2020-2023 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk/include/utk/ElapsedTimer.hpp
//
// Description: Provides definition of the ElapsedTimer class.


#ifndef UTK_INCLUDE_UTK_ELAPSEDTIMER_HPP
#define UTK_INCLUDE_UTK_ELAPSEDTIMER_HPP


#include <chrono>

#include "utk/namespace.hpp"


UTK_NAMESPACE_OPEN


template < class TClock >
class ElapsedTimer {
	using Clock     = TClock;
	using TimePoint = typename TClock::time_point;
	using Duration  = typename TClock::duration;


 public:
	TimePoint Start ()
	{
		start_time_point_ = clock_.now ();

		return start_time_point_;
	}


	TimePoint Restart ()
	{
		start_time_point_ = clock_.now ();

		return start_time_point_;
	}


	[[nodiscard]] Duration Elapsed ()
	{
		return clock_.now () - start_time_point_;
	}


 private:
	Clock     clock_ {};
	TimePoint start_time_point_ {};
};


UTK_NAMESPACE_CLOSE


#endif /* UTK_INCLUDE_UTK_ELAPSEDTIMER_HPP */
