// Copyright 2019-2023 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk/include/utk/lockfree/SPSCDoubleBufferedValue.hpp
//
// Description: Provides definition of the SPSCDoubleBufferedValue class.


#ifndef UTK_INCLUDE_UTK_LOCKFREE_SPSCDOUBLEBUFFEREDVALUE_HPP
#define UTK_INCLUDE_UTK_LOCKFREE_SPSCDOUBLEBUFFEREDVALUE_HPP


#include <atomic>

#include "utk/namespace.hpp"


UTK_NAMESPACE_OPEN


/**
   @brief Double-buffered value storage for single producer single consumer
   concurrent system

   @details Reading and writing of value is lock-free and wait-free. The same
   value may be read multiple times in case where there is no new value present
   or there is an ongoing write operation.

   @note Implementation is based on ideas from
   https://stackoverflow.com/questions/23666069/single-producer-single-consumer-data-structure-with-double-buffer-in-c

   @todo Improve implementation. Use less condition operators. Improve usage of
   memory access order.
*/
template < class TValueType >
class SPSCDoubleBufferedValue {
	enum {
		kReadInProgressOffset  = 1,
		kReadInProgress        = 0x01 << kReadInProgressOffset,
		kWriteInProgressOffset = 2,
		kWriteInProgress       = 0x01 << kWriteInProgressOffset,
		kNewInCell_0_Offset    = 3,
		kNewInCell_0           = 0x01 << kNewInCell_0_Offset,
		kNewInCell_1_Offset    = 4,
		kNewInCell_1           = 0x01 << kNewInCell_1_Offset,
		kWriteToCell_0         = 0x00,
		kWriteToCell_1         = 0x01,
		kReadFromCell_0        = 0x00,
		kReadFromCell_1        = 0x01
	};


 public:
	using ValueType = TValueType;


	SPSCDoubleBufferedValue ()
	    : state_ {kWriteToCell_0}
	{
	}


	SPSCDoubleBufferedValue (ValueType const& i_value)
	    : state_ {kWriteToCell_0}
	    , slots_ {i_value, i_value, i_value}
	{
	}


	operator ValueType () const
	{
		int state {state_.fetch_or (kReadInProgress)};

		int index {(state ^ kWriteToCell_1) & kReadFromCell_1};

		// If no new value is available, read the most recent value.
		if ((0 == (state & kNewInCell_0)) && (0 == index))
		{
			index = 2;
		}
		else if ((0 == (state & kNewInCell_1)) && (1 == index))
		{
			index = 2;
		}

		auto const value {slots_[ index ]};

		if (2 != index)
		{
			slots_[ 2 ] = value;
		}

		state = state_;

		int new_state {state & (~kReadInProgress)};

		/*
		  The second condition prevents re-reading of old values.
		*/
		if (!(state & kWriteInProgress) &&
		    (state & (kNewInCell_0 | kNewInCell_1)))
		{
			new_state ^= kWriteToCell_1;
		}

		state_.compare_exchange_strong (state, new_state);

		return value;
	}


	ValueType operator= (ValueType const& i_value)
	{
		int state {state_.fetch_or (kWriteInProgress)};

		int index {state & kWriteToCell_1};

		slots_[ index ] = i_value;

		state = state_;

		int new_state {state & (~kWriteInProgress)};

		switch (index)
		{
			case 0: {
				new_state |= kNewInCell_0;
				new_state &= ~kNewInCell_1;
			}
			break;

			case 1: {
				new_state |= kNewInCell_1;
				new_state &= ~kNewInCell_0;
			}
			break;
		}

		if (!(state & kReadInProgress))
		{
			new_state ^= kWriteToCell_1;
		}

		state_.compare_exchange_strong (state, new_state);

		return i_value;
	}


 private:
	mutable std::atomic_int state_;

	mutable ValueType slots_[ 3 ];
};


UTK_NAMESPACE_CLOSE


#endif /* UTK_INCLUDE_UTK_LOCKFREE_SPSCDOUBLEBUFFEREDVALUE_HPP */
