// Copyright 2019-2023 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name: utk/include/utk/io/rebind_ios.hpp
//
// Description: Declaration of the RebindIosToFile function.


#ifndef UTK_INCLUDE_UTK_IO_REBIND_IOS_HPP
#define UTK_INCLUDE_UTK_IO_REBIND_IOS_HPP


#include <filesystem>
#include <fstream>
#include <istream>
#include <ostream>

#include "utk/io/IosRaiiRebinder.hpp"

#include "utk/namespace.hpp"


UTK_NAMESPACE_OPEN


template < class TIos >
auto RebindIosToFile (
    ::std::filesystem::path const& i_file_name,
    TIos&                          io_ios,
    ::std::ios_base::openmode      i_open_mode = ::std::ios_base::out)
{
	using Ios           = TIos;
	using CharType      = typename Ios::char_type;
	using TraitsType    = typename Ios::traits_type;
	using StreamBufType = ::std::basic_filebuf< CharType, TraitsType >;

	static auto constexpr kIsIstream = ::std::
	    is_base_of_v< ::std::basic_istream< CharType, TraitsType >, Ios >;

	static auto constexpr kIsOstream = ::std::
	    is_base_of_v< ::std::basic_ostream< CharType, TraitsType >, Ios >;

	static auto constexpr kIsFstream = ::std::
	    is_base_of_v< ::std::basic_fstream< CharType, TraitsType >, Ios >;

	static_assert (
	    kIsIstream || kIsOstream || kIsFstream,
	    "Invalid stream type. Should be std::istream or std::ostream "
	    "decendant");

	auto constexpr io_mode =
	    (kIsIstream
	         ? ::std::ios_base::in
	         : (kIsOstream ? ::std::ios_base::out
	                       : (::std::ios_base::in | ::std::ios_base::out)));

	auto stream_buf = StreamBufType {};

	stream_buf.open (i_file_name, i_open_mode | io_mode);

	return IosRaiiRebinder< Ios > {io_ios, ::std::move (stream_buf)};
}


UTK_NAMESPACE_CLOSE


#endif /* UTK_INCLUDE_UTK_IO_REBIND_IOS_HPP */
