// Copyright 2019-2023 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name: utk/include/utk/io/rebind_cstream.hpp
//
// Description: Declaration of the RebindCStreamToFile function.


#ifndef UTK_INCLUDE_UTK_IO_REBIND_CSTREAM_HPP
#define UTK_INCLUDE_UTK_IO_REBIND_CSTREAM_HPP


#include <memory>
#include <string_view>

#include "utk/namespace.hpp"
#include "utk/utk_export.h"

#include "utk/io/CStreamRaiiRebinder.hpp"


UTK_NAMESPACE_OPEN


UTK_EXPORT CStreamRaiiRebinder RebindCStreamToFile (
    ::std::string_view const i_file_name,
    char const*              i_open_mode,
    FILE&                    io_stream);


UTK_NAMESPACE_CLOSE


#endif /* UTK_INCLUDE_UTK_IO_REBIND_CSTREAM_HPP */
