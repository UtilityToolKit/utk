// Copyright 2019-2023 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name: utk/include/utk/io/CStreamRaiiRebinder.hpp
//
// Description: Declaration of the CStreamRaiiRebinder class.


#ifndef UTK_INCLUDE_UTK_IO_CSTREAMRAIIREBINDER_HPP
#define UTK_INCLUDE_UTK_IO_CSTREAMRAIIREBINDER_HPP


#include <cstdio>
#include <memory>

#include "utk/namespace.hpp"
#include "utk/utk_export.h"


UTK_NAMESPACE_OPEN


class UTK_EXPORT CStreamRaiiRebinder {
 public:
	explicit CStreamRaiiRebinder (FILE& io_stream, FILE& i_new_stream);

	explicit CStreamRaiiRebinder (FILE& io_stream, FILE* i_new_stream);

	explicit CStreamRaiiRebinder (int i_old_fd, int i_new_fd);

	CStreamRaiiRebinder (CStreamRaiiRebinder&& i_src);

	CStreamRaiiRebinder (CStreamRaiiRebinder const&) = delete;

	~CStreamRaiiRebinder ();

	void RestoreBinding ();


 private:
	::std::unique_ptr< FILE, decltype (&::std::fclose) > new_stream_;

	FILE* rebound_stream_ = nullptr;
	int   rebound_fd_;
	int   original_dup_;
	bool  is_active_ = true;
};


UTK_NAMESPACE_CLOSE


#endif /* UTK_INCLUDE_UTK_IO_CSTREAMRAIIREBINDER_HPP */
