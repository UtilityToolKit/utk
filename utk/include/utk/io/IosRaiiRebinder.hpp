// Copyright 2019-2023 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name: utk/include/utk/io/IosRaiiRebinder.hpp
//
// Description: Declaration of the IosRaiiRebinder class.


#ifndef UTK_INCLUDE_UTK_IO_IOSRAIIREBINDER_HPP
#define UTK_INCLUDE_UTK_IO_IOSRAIIREBINDER_HPP


#include <ios>          // ios_base
#include <streambuf>    // basic_streambuf
#include <type_traits>  // enable_if_v, is_base_v

#include "utk/namespace.hpp"


UTK_NAMESPACE_OPEN


template < class TIos >
class IosRaiiRebinder {
 public:
	using Ios        = TIos;
	using CharType   = typename Ios::char_type;
	using TraitsType = typename Ios::traits_type;
	using StreamBuf  = ::std::basic_streambuf< CharType, TraitsType >;


	template <
	    class TRebindStreamBuf_,
	    class = ::std::enable_if_t<
	        ::std::is_base_of_v< StreamBuf, TRebindStreamBuf_ > > >
	explicit IosRaiiRebinder (Ios& io_ios, TRebindStreamBuf_ i_stream_buf)
	    : is_active_ {true}
	    , ios_ {io_ios}
	    , rebind_stream_buf_ {::std::make_unique< TRebindStreamBuf_ > (
	          ::std::move (i_stream_buf))}
	    , original_streambuf_ {io_ios.rdbuf ()}
	{
		ios_.rdbuf (rebind_stream_buf_.get ());
	}


	explicit IosRaiiRebinder (Ios& io_ios, StreamBuf* i_stream_buf = nullptr)
	    : is_active_ {true}
	    , ios_ {io_ios}
	    , original_streambuf_ {io_ios.rdbuf ()}
	{
		ios_.rdbuf (i_stream_buf);
	}


	IosRaiiRebinder (IosRaiiRebinder&& i_src)
	    : is_active_ {i_src.is_active_}
	    , ios_ {i_src.ios_}
	    , rebind_stream_buf_ {::std::move (i_src.rebind_stream_buf_)}
	    , original_streambuf_ {i_src.original_streambuf_}
	{
		i_src.is_active_ = false;
	}


	~IosRaiiRebinder ()
	{
		RestoreBinding ();
	}


	void RestoreBinding ()
	{
		if (is_active_)
		{
			ios_.rdbuf (original_streambuf_);

			is_active_ = false;
		}
	}


 private:
	IosRaiiRebinder (IosRaiiRebinder const&) = delete;


	bool                           is_active_;
	Ios&                           ios_;
	::std::unique_ptr< StreamBuf > rebind_stream_buf_;
	StreamBuf*                     original_streambuf_;
};


UTK_NAMESPACE_CLOSE


#endif /* UTK_INCLUDE_UTK_IO_IOSRAIIREBINDER_HPP */
