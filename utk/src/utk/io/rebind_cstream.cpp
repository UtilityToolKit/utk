// Copyright 2019-2023 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk/src/utk/io/rebind_cstream.cpp
//
// Description: Provides definition of the RebindCStreamToFile function.


#include "utk/io/rebind_cstream.hpp"

#ifdef _MSC_VER
#	include <io.h>
#endif


#include <iostream>


UTK_NAMESPACE_OPEN


CStreamRaiiRebinder RebindCStreamToFile (
    ::std::string_view const i_file_name,
    char const*              i_open_mode,
    FILE&                    io_stream)
{
#ifdef _MSC_VER
	if (-2 == _fileno (&io_stream))
	{
		freopen (i_file_name.data (), i_open_mode, &io_stream);
	}
#endif

	return CStreamRaiiRebinder {
	    io_stream, ::std::fopen (i_file_name.data (), i_open_mode)};
}


UTK_NAMESPACE_CLOSE
