// Copyright 2019-2023 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk/src/utk/io/CStreamRaiiRebinder.cpp
//
// Description: Provides definition of the CStreamRaiiRebinder class
//              member functions.


#include "utk/io/CStreamRaiiRebinder.hpp"


#include <iostream>


#ifdef _MSC_VER
#	include <io.h>
#	include <Windows.h>

#	define close _close
#	define dup _dup
#	define dup2 _dup2
#	define fileno _fileno
#	define fsync _commit
#else
#	include <unistd.h>
#endif


UTK_NAMESPACE_OPEN


CStreamRaiiRebinder::CStreamRaiiRebinder (FILE& io_stream, FILE& i_new_stream)
    : new_stream_ {nullptr, &::std::fclose}
    , rebound_stream_ {&io_stream}
    , rebound_fd_ {fileno (&io_stream)}
    , original_dup_ {dup (fileno (&io_stream))}
{
	dup2 (fileno (&i_new_stream), rebound_fd_);
}


CStreamRaiiRebinder::CStreamRaiiRebinder (FILE& io_stream, FILE* i_new_stream)
    : new_stream_ {i_new_stream, &::std::fclose}
    , rebound_stream_ {&io_stream}
    , rebound_fd_ {fileno (&io_stream)}
    , original_dup_ {dup (fileno (&io_stream))}
{
	dup2 (fileno (i_new_stream), rebound_fd_);
}


CStreamRaiiRebinder::CStreamRaiiRebinder (int i_old_fd, int i_new_fd)
    : new_stream_ {nullptr, &::std::fclose}
    , rebound_fd_ {i_old_fd}
    , original_dup_ {dup (i_old_fd)}
    , is_active_ {true}
{
	dup2 (i_new_fd, rebound_fd_);
}


CStreamRaiiRebinder::CStreamRaiiRebinder (CStreamRaiiRebinder&& i_src)
    : new_stream_ {::std::move (i_src.new_stream_)}
    , rebound_fd_ {i_src.rebound_fd_}
    , original_dup_ {i_src.original_dup_}
    , is_active_ {i_src.is_active_}
{
	i_src.is_active_ = false;
}


CStreamRaiiRebinder::~CStreamRaiiRebinder ()
{
	RestoreBinding ();
}


void CStreamRaiiRebinder::RestoreBinding ()
{
	if (is_active_)
	{
		if (rebound_stream_)
		{
			::std::fflush (rebound_stream_);
		}

		fsync (rebound_fd_);

		dup2 (original_dup_, rebound_fd_);

		is_active_ = false;

		close (original_dup_);
	}
}


UTK_NAMESPACE_CLOSE
